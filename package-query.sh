#!/bin/zsh
#
# Run "apt-cache show" for a given query line, and wrap the result
# into XML

if [ -z "$HERE" ] ; then
    cd "$(dirname $(readlink -f $0))"
    export APT_CONFIG=whatsit.conf
else
    export APT_CONFIG=$HERE
fi

LOCK=/var/run/lock/pkginfo.lock

function urldecode() {
    echo -n "$(echo -n "$1" | sed 's/%\([0-9A-F][0-9A-F]\)/\\x\1/g;')";
}

Q="${${(M@)${(s[&])QUERY_STRING}:#q=*}#q=}"
Q="$(urldecode "$Q")"

date +"%Y-%m-%d %H:%M:%S $REMOTE_ADDR package-query Q=$Q" >> /tmp/pkginfo-x.log

coproc flock -s $LOCK apt-cache show "$Q"
exec {TIMEOUT}< <( ( sleep 4 ; echo TIMEOUT ) & )

function urlencode() {
    echo -n "${${${1//&/&amp;}//</&lt;}//>/&gt;}"
}

# package version => codename
function firstsuite() {
    grep -A 1 "Package: $1" var/lib/apt/lists/*Packages | \
	grep -B1 "$2" | sed 's/.*_dists_//;s/_.*//;q'
}

function suite_repo() {
    echo "suite_repo $1" >> /tmp/pkginfo-x.log
    case "$1" in
	ascii|jessie) echo "archive.devuan.org/merged" ;;
	*) echo "deb.devuan.org/merged" ;;
    esac
}

DEBFILE=
# key value -- process value part for some keys
# Breaks Conflicts Depends Pre-Depends Provides Replaces Recommends Suggests 
function subdetail() {
    local P V
    echo -n "<value>"
    case "$1" in
	Maintainer)
	    echo -n "<maintainer>$(urlencode "$2")</maintainer>"
	    ;;
	Homepage)
	    echo -n "<homepage>$(urlencode "$2")</homepage>"
	    ;;
	Filename)
	    case "$2" in
		pool/DEBIAN*|pool/DEVUAN*)
		    DEBFILE="http://$(suite_repo $SUITE)/$2"
		    echo -n "<merged><repo>http://$(suite_repo $SUITE)</repo><path>$2</path></merged>"
		    ;;
		*)
		    echo -n "<devuan>$2</devuan>"
		    DEBFILE="http://deb.devuan.org/devuan/$2"
		    ;;
	    esac
	    ;;
	Breaks|Conflicts|Depends|Pre-Depends|Provides|Replaces|Recommends|Suggests)
	    for ITEM in ${(s[,])2} ; do
		echo "<item>"
		for REF in ${(s[|])ITEM} ; do
		    REF=( ${(s[ ])REF} )
		    P="$REF[1]"
		    V="$REF[2,-1]"
		    cat <<EOF
<ref><pkg>$P</pkg><ver>${"$(urlencode "$V")"/ /&#160;}</ver></ref>"
EOF
		done
		echo "</item>"
	    done
	    ;;
	*)
	    urlencode "$2"
	    ;;
    esac
}

# Select post processing
if [ "${0##*.}" != xml ] ; then
    POSTPROC=( xsltproc --stringparam EXT html )
    CT="text/html"
else
    POSTPROC=( cat )
    CT="text/xml"
fi

[ -z "$HERE" ] && cat <<EOF
Content-Type: $CT                                                               
Expires: $(date -R -d "+10 minutes")

EOF

{
    cat <<EOF
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="templates/package-query.xsl"?>
<pkgdetail>
<name>$Q</name>
EOF

    if [ -n "$Q" ] ; then
	cat <<EOF
<query>${${Q//</&lt;}//>/&gt;}</query>
EOF
    fi

    echo "<results>"

    function listcontent() {
	local SUITE DATA
	SUITE="$2"
	DATA=(
	    #${(f)"$(flock -s $LOCK apt-file list --filter-suites="$SUITE" "$1")"}
	)
	echo "<content>"
	wget --timeout 6 -4 -q -O - "$DEBFILE" | dpkg-deb -c - | 
	    while read -r a b c d e f g ; do
		[[ "$f" == */ ]] && continue
		cat <<EOF
<pathname>${f#.}</pathname>
EOF
	    done | sort
	echo "</content>"
    }

    PACKAGE=
    KEY=
    TIMEDOUT=
    VERSION=
    SUITE=
    while IFS= read -rp D ; do
	[ -z "$D" ] && continue
	if [ "$D[1]" = " " ] ; then
	    echo -n "\n${${${D//</&lt;}//>/&gt;}//&/&amp;}"
	    continue;
	fi
	[ -n "$KEY" ] && echo "</value>\n</detail>"
	KEY="${D%%:*}"
	if [ "$KEY" = "Version" ] ; then
	    VERSION="${D#*: }"
	    SUITE="$(firstsuite "$PACKAGE" "$VERSION")"
	fi
	if [ "$KEY" = "Package" ] ; then
	    if [ -n "$PACKAGE" ] ; then
		[ -n "$VERSION" ] && listcontent $PACKAGE $SUITE $DEBFILE
		echo "</package>"
	    fi
	    PACKAGE="${D#*: }"
	    VERSION=
	    if read -u $TIMEOUT -t xx ; then
		echo TIMEOUT >&2
		TIMEDOUT=yes
		PACKAGE=
		KEY=
		break
	    fi
	    echo "<package>"
	fi
	echo "<detail>\n<key>$KEY</key>"
	subdetail "$KEY" "${D#*: }"
    done
    [ -n "$KEY" ] && echo "</value>\n</detail>"
    if [ -n "$PACKAGE" ] ; then
	[ -n "$VERSION" ] && listcontent $PACKAGE $SUITE $DEBFILE
	echo "</package>"
    fi

    [ -z "$TIMEDOUT" ] || echo "<timeout/>"

    cat <<EOF
</results>
</pkgdetail>
EOF
} | $POSTPROC -

true
