<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="EXT">xml</xsl:param>
  <xsl:variable
      name="CSS">https://beta.devuan.org/ui/css/pkginfo</xsl:variable>
  <xsl:variable name="HEADER">
    <div class="nav">
      <div class="leftfl">
	<a href="https://pkginfo.devuan.org" title="Devuan Packages">
	<span>Home</span></a>
	<span class="separator">|</span>
	<a href="https://devuan.org" title="Devuan website">
	<span>Devuan</span></a>
	<span class="separator">|</span>
	<a href="https://git.devuan.org" title="Devuan gitlab">
	<span>Git</span></a>
	<span class="separator">|</span>
	<a href="https://dev1galaxy.org" title="Devuan forum">
	<span>Forum</span></a>
	<span class="separator">|</span>
	<a href="https://bugs.devuan.org" title="Devuan Bugs Tracker">
	<span>Bugs</span></a>
	<span class="separator">|</span>
	<a href="https://popcon.devuan.org" title="Popularity Context">
	<span>Popcon</span></a>
      </div>

      <div class="rightfl-do">
	<a title="Please support Devuan development" class="button-do"
	   href="https://www.devuan.org/os/donate">Donate now!</a>
      </div>

      <div class="rightfl-dl">
	<a title="Download Devuan" class="button-dl"
	   href="https://files.devuan.org/">Download</a>
      </div>

    </div>
    <div id="titleblock">
      <h1 id="title">Devuan Package Information</h1>
      <div id="guide">
	Use * for globbing, or .* and friends for regex.
	(<a
	href="https://unix.stackexchange.com/questions/506921/what-kind-of-pattern-matching-does-apt-cache-allow-for-specifying-package-names"
	target="blank" >help</a>)
	<span style="width:2em;display:inline-block;"> </span>
	[<a href="/sources.list.txt">sources.list used by this service</a>]
      </div>
      <xsl:copy-of select="$FORM" />
    </div>
  </xsl:variable>

  <xsl:variable name="FORM">
    <div id="policyform">
      <form method="GET" action="policy-query.{$EXT}">
	<div id="selector">
	  <select name="c">
	    <option value="package">Package pattern:</option>
	    <option value="file">File pattern:</option>
	  </select>
	</div>
	<xsl:element name="input">
	  <xsl:attribute name="type">textfield</xsl:attribute>
	  <xsl:attribute name="name">q</xsl:attribute>
	  <xsl:attribute name="value"><xsl:value-of
	  select="query"/></xsl:attribute>
	</xsl:element>
	<xsl:element name="input">
	  <xsl:attribute name="type">submit</xsl:attribute>
	  <xsl:attribute name="name">x</xsl:attribute>
	  <xsl:attribute name="value">submit</xsl:attribute>
	</xsl:element>
      </form>
    </div>
  </xsl:variable>

  <xsl:variable name="FOOTER">
    <p style="margin-top:0px; margin-bottom:10px ; text-align: center;">
      This site is free of cookies and javascript
    </p>
    <div class="foot">                   
      <p id="copyright" style="white-space: nowrap;">
        Copyright (c) 2020-2024
	<a href="https://www.dyne.org" data-rel="fiscal sponsor"
           title="Learn about our fiscal sponsor"> Dyne.org foundation</a>
        <a rel="license"
	   href="https://creativecommons.org/licenses/by-sa/4.0/" 
           title="This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.">CC-BY-SA 4.0 international</a>
      </p>
      <p class="trademarks">
	Devuan is a registered trademark of the Dyne.org foundation.
      </p>
      <p class="trademarks" style="margin-top: -15px;">
	Debian is a registered trademark of Software in the Public
	Interest, Inc. Linux is a registered trademark of Linus
	Torvalds.
      </p>
    </div>
  </xsl:variable>

  <xsl:variable name="DEBTREEFORM">
    <div id="debtree-button-bar">
      <div id="debtree-button">
	<xsl:element name="a">
	  <xsl:attribute
	      name="href">debtree-query.<xsl:copy-of
	      select="$EXT"/>?c=package&amp;q=<xsl:copy-of
	      select="$PACKAGE" /></xsl:attribute>
	  <xsl:copy-of select="$PACKAGE" />
	  debtree
	</xsl:element>
      </div>
    </div>
  </xsl:variable>

</xsl:stylesheet>
