<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="common.xsl" />
  <xsl:variable name="PACKAGE"></xsl:variable>
  <xsl:template match="/pkginfo">
    <html>
      <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>Devuan Package View</title>
      <link rel="stylesheet" type="text/css" href="{$CSS}/common.css"/>
      <link rel="stylesheet" type="text/css" href="{$CSS}/policy-query.css"/>
      </head>
      <body>
	<xsl:copy-of select="$HEADER" />
	<div id="text" class="result">
	  <div class="query">
	    Results for package pattern:
	    <span class="given"><xsl:value-of select="query"/></span>
	  </div>
	  <xsl:for-each select="timeout">
	    <div class="timeout">
	      Search timed out. Please use a more specific pattern.
	    </div>
	  </xsl:for-each>
	  <xsl:for-each select="package"><xsl:sort select="name"/>
	  <div class="package">
	    <div class="name"><xsl:value-of select="name"/></div>
	    <xsl:for-each select="version"><div class="version">
	      <div class="code">
		<xsl:element name="a">
		  <xsl:attribute
		      name="href"
		      >package-query.<xsl:copy-of
		      select="$EXT"/>?c=package&amp;q=<xsl:value-of
		      select="../name"/>=<xsl:value-of
		      select="code"/></xsl:attribute>
		  <xsl:value-of select="code"/>
		</xsl:element>
	      </div>
	      <xsl:for-each select="source">
		<div class="source">
		  <div class="host"><xsl:value-of select="host"/> </div>
		  <xsl:text> </xsl:text>
		  <div class="section"><xsl:value-of select="section"/> </div>
		  <xsl:text> </xsl:text>
		  <div class="arch"><xsl:value-of select="arch"/> </div>
		</div>
	      </xsl:for-each>
	    </div></xsl:for-each>
	  </div></xsl:for-each>
	</div>
	<xsl:for-each select="frontmatter">
	  <xsl:copy-of select="document('../description.xml')/doc" />
	</xsl:for-each>
	<xsl:copy-of select="$FOOTER" />
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
