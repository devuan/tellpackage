<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="common.xsl" />
  <xsl:variable name="PACKAGE"></xsl:variable>
  <xsl:template match="/result">
    <html>
      <head><title>Devuan Package View</title>
      <link rel="stylesheet" type="text/css" href="{$CSS}/common.css"/>
      <link rel="stylesheet" type="text/css" href="{$CSS}/debtree-query.css"/>
      </head>
      <body>
	<xsl:copy-of select="$HEADER" />
	<hr />
	<div id="text" class="result">
	  <div id="content">
	    <div id="query">
	      <xsl:value-of select="query" /> "debtree" diagram
	    </div>
	    <div class="diagram">
	      <xsl:apply-templates />
	    </div>
	  </div>
	</div>
	<xsl:copy-of select="$FOOTER" />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()[name() != 'xml-stylesheet']">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="query">
  </xsl:template>

</xsl:stylesheet>
