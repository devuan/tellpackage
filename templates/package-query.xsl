<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="common.xsl" />
  <xsl:variable name="PACKAGE"><xsl:value-of
  select="/pkgdetail/results/package/detail/value" /></xsl:variable>
  <xsl:template match="/pkgdetail">
    <html>
      <head><title>Devuan Package View</title>
      <link rel="stylesheet" type="text/css" href="{$CSS}/common.css"/>
      <link rel="stylesheet" type="text/css" href="{$CSS}/package-query.css"/>
      </head>
      <body>
	<xsl:copy-of select="$HEADER" />
	<div id="text" class="result">
	  <div class="query">
	    Results for package pattern:
	    <span class="given"><xsl:value-of select="query"/></span>
	  </div>
	  <xsl:for-each select="results/timeout">
	    <div class="timeout">
	      Search timed out. Please use a more specific pattern.
	    </div>
	  </xsl:for-each>
	  <xsl:copy-of select="$DEBTREEFORM" />
	  <xsl:for-each select="results/package">
	    <div class="package">
	      <xsl:for-each select="detail"><div class="detail">
		<div class="key"><xsl:value-of select="key"/>: </div>
		<div class="value">
		  <xsl:for-each select="value">
		    <xsl:choose>
		      <xsl:when test="maintainer">
			<xsl:element name="a">
			  <xsl:attribute name="href">mailto:<xsl:value-of select="."/></xsl:attribute>
			  <xsl:value-of select="."/>
			</xsl:element>
		      </xsl:when>
		      <xsl:when test="homepage">
			<xsl:element name="a">
			  <xsl:attribute name="href"><xsl:value-of select="."/></xsl:attribute>
			  <xsl:value-of select="."/>
			</xsl:element>
		      </xsl:when>
		      <xsl:when test="devuan">
			<xsl:element name="a">
			  <xsl:attribute name="href">http://deb.devuan.org/devuan/<xsl:value-of select="."/></xsl:attribute>
			  <xsl:value-of select="."/>
			</xsl:element>
		      </xsl:when>
		      <xsl:when test="merged">
			<xsl:element name="a">
			  <xsl:attribute name="href"><xsl:value-of select="merged/repo"/>/<xsl:value-of select="merged/path"/></xsl:attribute>
			  <xsl:value-of select="merged/path"/>
			</xsl:element>
		      </xsl:when>
		      <xsl:when test="item">
			<xsl:for-each select="item">
			  <xsl:if test="position()>1">, </xsl:if>
			  <xsl:for-each select="ref">
			    <xsl:if test="position()>1"> | </xsl:if>
			    <xsl:element name="a">
			      <xsl:attribute
			      name="href">policy-query.<xsl:copy-of
			      select="$EXT"/>?c=package&amp;q=<xsl:value-of
			      select="pkg"/></xsl:attribute>
			      <xsl:value-of select="pkg"/>
			    </xsl:element>
			    <xsl:if test="ver != ''">
			      <xsl:text>&#160;</xsl:text>
			      <xsl:value-of select="ver"/>
			    </xsl:if>
			  </xsl:for-each>
			</xsl:for-each>
		      </xsl:when>
		      <xsl:otherwise>
			<xsl:value-of select="."/>
		      </xsl:otherwise>
		    </xsl:choose>
		  </xsl:for-each>
		</div>
	      </div></xsl:for-each>
	      <div id="filelist">
		<div id="filelist-title">
		  Content
		</div>
		<xsl:for-each select="content/pathname">
		  <div class="pathname">
		    <xsl:value-of select="."/>
		  </div>
		</xsl:for-each>
	      </div>
	    </div>
	  </xsl:for-each>
	</div>
	<xsl:copy-of select="$FOOTER" />
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
