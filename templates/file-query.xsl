<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="common.xsl" />
  <xsl:variable name="PACKAGE"></xsl:variable>
  <xsl:template match="/result">
    <html>
      <head><title>Devuan Package View</title>
      <link rel="stylesheet" type="text/css" href="{$CSS}/common.css"/>
      <link rel="stylesheet" type="text/css" href="{$CSS}/file-query.css"/>
      </head>
      <body>
	<xsl:copy-of select="$HEADER" />
	<div id="text" class="result">
	  <div class="query">
	    Results for file pattern:
	    <span class="given"><xsl:value-of select="query"/></span>
	  </div>
	  <xsl:for-each select="timeout">
	    <div class="timeout">
	      Search timed out. Please use a more specific pattern.
	    </div>
	  </xsl:for-each>
	  <xsl:for-each select="package"><div class="package">
	    <div class="name">
	      <xsl:element name="a">
		<xsl:attribute
		    name="href">policy-query.<xsl:copy-of
		    select="$EXT"/>?c=package&amp;q=<xsl:value-of
		    select="name" /></xsl:attribute>
		<xsl:value-of select="name"/>
	      </xsl:element>
	    </div>
	    <xsl:for-each select="pathname">
	      <div class="pathname">
		<xsl:value-of select="."/>
	      </div>
	    </xsl:for-each>
	  </div>
	  </xsl:for-each>
	</div>
	<xsl:copy-of select="$FOOTER" />
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
