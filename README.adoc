tellpackage
===========

This is a CGI application to provide package information using apt.

Currently available at https://pkginfo.devuan.org

Description
-----------

The Web interface includes a "search box" for filling in an argument
for an +apt-cache policy+ run in an +apt+ runtime context that
includes all current Devuan source points. The result of that run is
presented as given and including links for a package version detail
lookup using +apt-cache show+.

The package detail view includes links to perform new +pacakge-query+
lookups for related (dependent) packages, as well as for manually
downloading the +deb+ file concerned. There is also a button to
trigger a +debtree+ run for the (unversioned) package name to get the
resulting diagram presented as an inline svg image.

Note that +debtree+ is not strong in handling package versions and it
simply shows a graph involving the "latest" of everything rather than
a graph relating to the original (versioned) package view.

Thus, the CGI application runs three command lines:

----
  $ apt-cache policy "$QUERY"
  $ apt-cache show "$PACKAGE=$VERSION"
  $ debtree "$PACKAGE"
----

The command outputs are further filtered into bespoke
https://www.w3.org/TR/xml[XML 1.0] representations that become the
bases for the views that browsers typically render by applying the
nominated https://www.w3.org/TR/xslt-30[XSL 3.0] stylesheets.

In other words, a request handling flow is as follows:

 1. The request is first translated into its servicing script by means
 of a link from the interface name (e.g., package-query.xml or
 package-query.html) to the actual script (e.g., package-query.sh). As
 per the normal https://tools.ietf.org/html/rfc3875[CGI 1.1
 Specification] the query string is given to the script via the
 environment variable +QUERY_STRING+.

 2. The servicing script runs the actual command with its argument
 provided as detail for the +q+ parameter in the query string, and
 decorates the command output to make up an XML object tree that
 includes a stylesheet link for the applicable stylesheet (e.g.
 +css/package-query.xsl+).

 3. The XML file has an XSL association for translating the XML "tree"
 into an HTML tree. If the servicing cgi script is accessed via an
 ".html" filename, then the XSL processing is done by the server, and
 otherwise the "raw" XML is passed on for XSL processing by client
 browser.
+
Either way the server or client processes the associated XSL
stylesheet, to transform the bespoke XML into an
https://www.w3.org/TR/html401[HTML 4.01] document according to the
transformation rules. The HTML document is a bare structure of plain
elements with bespoke identities and class assignments. It also
includes nominations fo http://www.w3.org/TR/CSS2[CSS 2.1] files to
load for layout particulars and looks.

 4. The browser upon processing the HTML document discovers the
 CSS associations and therefore loads and processes the associated
 CSS stylesheets.

 5. Eventually the browser will map the rendered result to the display
 for the user to view.

Installation
------------
The scripts use a local +apt+ environment which consists of a special
purpose configuration file +whatis.conf+ that in particular nominates
a +RootDir+ that is the root directory for the pathnames to other
configuration files and working directories. Especially the
+etc/apt/sources.list.d/+ directory that contains the lists files of
source points for package collections.

The CGI application includes a background job (+./update.sh+) to keep
the local configuration up to date with the source points. This is
simply a regular +apt-get update+ run in the local +apt+ environment.

The interface side includes a "cgi directory" with links:

.the +cgi-bin/+ links
----
debtree-query.html -> ../../tellpackage/debtree-query.sh
debtree-query.xml  -> ../../tellpackage/debtree-query.sh
file-query.html    -> ../../tellpackage/file-query.sh
file-query.xml     -> ../../tellpackage/file-query.sh
package-query.html -> ../../tellpackage/package-query.sh
package-query.xml  -> ../../tellpackage/package-query.sh
policy-query.html  -> ../../tellpackage/policy-query.sh
policy-query.xml   -> ../../tellpackage/policy-query.sh
----

The +\*.xml+ links implement client-side XSLT access, and the
+\*.html+ links implement server-side XSLT access.

The app uses CSS from +https://beta.devuan.org/ui/css/pkginfo+ which
is hard-coded into +templates/common.xsl+.

nginx configuration fragments
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

====
    # This rewrite preserves the now outdated access URLs
    rewrite ^/xsl-bin/(.*)$ /cgi-bin/$1 permanent;

    # Redirect blank access to /index.html
    location ~ ^/$ {
        return 301 https://$server_name/index.html;
    }

    # Redirect /index.html to an empty policy-query
    location /index.html {
        return 301 https://$server_name/cgi-bin/policy-query.html;
    }

    # Serve xsl files verbatim
    location /cgi-bin/templates/ {
	root /home/pkginfo.devuan.org/tellpackage;
        rewrite ^/cgi-bin/(.*)$ /$1 break;
    }

    # Serve description.xml verbatim (for client-side xslt)
    location /cgi-bin/description.xml {
        root /home/pkginfo.devuan.org/tellpackage;
        rewrite ^/cgi-bin/(.*)$ /$1 break;
    }

    # Serve /cgi-bin/ via the web app
    location /cgi-bin/ {
        gzip off;
        autoindex off;
	root  /home/pkginfo.devuan.org/public;
        # Fastcgi socket
        fastcgi_pass  unix:/var/run/fcgiwrap.socket;
        # Fastcgi parameters, include the standard ones
	include /etc/nginx/fastcgi_params;
    }
====
