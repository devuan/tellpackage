#!/bin/bash
#

if [ -z "$HERE" ] ; then
    HERE="$(dirname "$(readlink -f -- "$0")")/whatsit.conf"
fi
export APT_CONFIG=$HERE

LOCK=/var/run/lock/pkginfo.lock

trusted_dir="$(tr -d '";' < $APT_CONFIG | awk '$1=="RootDir" {print $2;}')"
trusted_dir+="/etc/apt/trusted.gpg.d"

{
    flock -e 9
    mkdir -p "$trusted_dir"
    dpkg -L devuan-keyring 2>&1 | grep -E '^/etc/apt.*\.gpg$' | \
	xargs -I{} cp '{}' "$trusted_dir"
    apt-file update
} 9>$LOCK
