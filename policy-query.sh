#!/bin/zsh
#
# Run "apt-cache policy" for a given query line, and wrap the result
# into XML

if [ -z "$HERE" ] ; then
    cd "$(dirname $(readlink -f $0))"
    export APT_CONFIG=whatsit.conf
else
    export APT_CONFIG=$HERE
fi

LOCK=/var/run/lock/pkginfo.lock

function urldecode() {
    echo -n "$(echo -n "$1" | sed 's/%\([0-9A-F][0-9A-F]\)/\\x\1/g;')";
}

C="${${(M@)${(s[&])QUERY_STRING}:#c=*}#c=}"
case "$C" in
    file) exec ./file-query.sh ;;
    *) : ;;
esac

Q="${${(M@)${(s[&])QUERY_STRING}:#q=*}#q=}"
Q="$(urldecode "$Q")"


date +"%Y-%m-%d %H:%M:%S $REMOTE_ADDR policy-query Q=$Q" >> /tmp/pkginfo-x.log

coproc flock -s $LOCK apt-cache policy "$Q"
exec {TIMEOUT}< <( ( sleep 2 ; echo TIMEOUT ) & )

# host section arch
function mksource() {
    echo "${1//pkgmaster/deb} $2 $3"
}

# package code source...
function end_version() {
    local C P
    P=$1
    C="$2"
    shift 2
    cat <<EOF
<version>
<code>$C</code>
EOF
    for S in $* ; do
	SX=( $(echo $S) )
	cat <<EOF
<source>
<host>$SX[1]</host>
<section>$SX[2]</section>
<arch>$SX[3]</arch>
</source>
EOF
    done
    echo "</version>"
}

# name 
function end_package() {
    echo "</package>"
}

PACKAGES=( )
PACKAGE=
VERSION=

# Select post processing
if [ "${0##*.}" != xml ] ; then
    POSTPROC=( xsltproc --stringparam EXT html )
    CT="text/html"
else
    POSTPROC=( cat )
    CT="text/xml"
fi

[ -z "$HERE" ] && cat <<EOF
Content-Type: $CT                                                               
Expires: $(date -R -d "+10 minutes")

EOF

{
    cat <<EOF
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="templates/policy-query.xsl"?>
<pkginfo>
EOF

    if [ -n "$Q" ] ; then
	cat <<EOF
<query>${${Q//</&lt;}//>/&gt;}</query>
EOF
    fi

    while IFS= read -rp D ; do
	if [ "${D[1]}" != " " ] ; then
	    # New package
	    if [ -n "$VERSION" ] ; then
		end_version $PACKAGE "$VERSION" "${SOURCES[@]}"
	    fi
	    if read -u $TIMEOUT -t xx ; then
		echo TIMEOUT >&2
		TIMEDOUT=yes
		break
	    fi
	    if [ -n "$PACKAGE" ] ; then
		end_package $PACKAGE
	    fi
	    PACKAGE="${D[1,-2]}"
	    cat <<EOF
<package><name>$PACKAGE</name>
EOF
	    VERSION=
	    continue
	fi
	W=( $(echo $D) )
	case ${W[1]} in
	    Installed:|Candidate:|Version) # ignore
		: ;;
	    -1) # add source point
		SOURCES+=( "$(mksource $W[2] $W[3] $W[4])" )
		;;
	    *) # Start another version block
		if [ -n "$VERSION" ] ;then
		    end_version $PACKAGE "$VERSION" "${SOURCES[@]}"
		fi
		VERSION="$W[1]"
		SOURCES=( )
		;;
	esac
    done

    if [ -n "$VERSION" ] ; then
	end_version $PACKAGE "$VERSION" "${SOURCES[@]}"
    fi

    if [ -n "$PACKAGE" ] ; then
	end_package $PACKAGE
    else
	echo "<frontmatter/>"
    fi

    [ -z "$TIMEDOUT" ] || echo "<timeout/>"

    echo "</pkginfo>"
} | $POSTPROC -
