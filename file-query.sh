#!/bin/zsh
#
# Run "apt-file find" for a given query line, and wrap the result into
# XML

if [ -z "$HERE" ] ; then
    cd "$(dirname $(readlink -f $0))"
    export APT_CONFIG=whatsit.conf
else
    export APT_CONFIG=$HERE
fi

function urldecode() {
    echo -n "$(echo -n "$1" | sed 's/%\([0-9A-F][0-9A-F]\)/\\x\1/g;')";
}

LOCK=/var/run/lock/pkginfo.lock

Q="${${(M@)${(s[&])QUERY_STRING}:#q=*}#q=}"
Q="$(urldecode "$Q")"

date +"%Y-%m-%d %H:%M:%S $REMOTE_ADDR file-query Q=$Q" >> /tmp/pkginfo-x.log

coproc flock -s $LOCK apt-file find "$Q"
COP=$!
exec {TIMEOUT}< <(
    ( sleep 8 ; pkill -P $COP -f apt-file ; echo TIMEOUT ) 2> /dev/null &
)

# Select post processing
if [ "${0##*.}" != xml ] ; then
    POSTPROC=( xsltproc --stringparam EXT html )
    CT="text/html"
else
    POSTPROC=( cat )
    CT="text/xml"
fi

[ -z "$HERE" ] && cat <<EOF
Content-Type: $CT                                                               
Expires: $(date -R -d "+10 minutes")

EOF

{
    cat <<EOF
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="templates/file-query.xsl"?>
<result>
EOF

    if [ -n "$Q" ] ; then
	cat <<EOF
<query>${${Q//</&lt;}//>/&gt;}</query>
EOF
    fi

    PACKAGE=
    while read -p P F ; do
	if [ "$P" != "$PACKAGE" ] ; then
	    [ -n "$PACKAGE" ] && echo "</package>"
	    echo "<package><name>${P%:}</name>"
	    PACKAGE="${P%:}"
	fi
	echo "<pathname>$F</pathname>"
    done
    [ -n "$PACKAGE" ] && echo "</package>"

    read -u $TIMEOUT -t xx && echo "<timeout/>"

    echo "</result>"
} | $POSTPROC -

true
