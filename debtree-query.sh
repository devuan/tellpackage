#!/bin/zsh
#
# Run "debtree" for a given query line, and pipe trough dot to generate svg

if [ -z "$HERE" ] ; then
    cd "$(dirname $(readlink -f $0))"
    export APT_CONFIG=whatsit.conf
else
    export APT_CONFIG=$HERE
fi

LOCK=/var/run/lock/pkginfo.lock

function urldecode() {
    echo -n "$(echo -n "$1" | sed 's/%\([0-9A-F][0-9A-F]\)/\\x\1/g;')";
}

Q="${${(M@)${(s[&])QUERY_STRING}:#q=*}#q=}"
Q="$(urldecode "$Q")"

date +"%Y-%m-%d %H:%M:%S $REMOTE_ADDR debtree-query Q=$Q" >> /tmp/pkginfo-x.log

# Select post processing
if [ "${0##*.}" != xml ] ; then
    POSTPROC=( xsltproc --stringparam EXT html )
    CT="text/html"
    DOT=(
	-Nhref='debtree-query.html?c=package&amp;q=\N'
    )
else
    POSTPROC=( cat )
    CT="text/xml"
    DOT=(
	-Nhref='debtree-query.xml?c=package&amp;q=\N'
    )
fi

[ -z "$HERE" ] && cat <<EOF
Content-Type: $CT                                                               
Expires: $(date -R -d "+10 minutes")

EOF

{
    cat <<EOF
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="templates/debtree-query.xsl"?>
<result>
<query>$Q</query>
EOF

    DEBTREE=(
	--no-skip
	--with-suggests
	--max-depth=3
	--no-alternatives
    )

    debtree $DEBTREE "$Q" | dot -Tsvg $DOT | tail -n +7

    cat <<EOF
</result>
EOF
} | $POSTPROC -

true
